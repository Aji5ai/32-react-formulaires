# React - Les formulaires

Dans ce brief, nous allons apprendre à créer des formulaires dans une application React.

## Ressources

- [React - Formulaires](https://fr.legacy.reactjs.org/docs/forms.html#gatsby-focus-wrapper)
- [React - Contrôler ou pas les formulaires](https://www.julienpradet.fr/fiches-techniques/to-control-or-not-to-control-forms-react/)
- [useRef() un hook pour contrôler le DOM dans React](https://react.dev/reference/react/useRef#manipulating-the-dom-with-a-ref)
- [React hook forms](https://react-hook-form.com/get-started)

## Contexte du projet

Nous allons commencer par créer un formulaire pas à pas.

Ensuite, tu créeras ton propre formulaire.

### 1 - Création d'un Composant de Formulaire de Base

Nous allons d'abord apprendre à créer un simple formulaire en React.

Voici un exemple simple :

```js
import React, { useState } from 'react';

function BasicForm() {
  // on prépare une valeur de state 'value' avec useState.
  // setValue() sera appelé à chaque changement de valeur de l'input
  const [value, setValue] = useState('');

  // La fonction handleSubmit() sera appelée lors de la validation du formulaire
  const handleSubmit = (event) => {
    event.preventDefault();
    alert(`Le texte saisi est : ${value}`);
  };

  // Et voilà le formulaire
  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      <button type="submit">Envoyer</button>
    </form>
  );
}

export default BasicForm;
```

Dans ce formulaire, le champ est contrôlé par React : on lui assigne une valeur gérée par useState(), et on utilise l'événement `onChange` pour mettre à jour cette valeur à chaque frappe du clavier.
Dans ce cas, chaque caractère entré dans le champ provoque une mise à jour de l'affichage (*re-rendering*).

### 2 - Validation des Données du Formulaire

Il est normal de valider toute entrée utilisateur dans des formulaires. C'est une question de sécurité : les inputs sont un point d'entrée dans le code de ton application pour les hackers.

Pour cela, nous devons créer des fonctions de validation.

Ajoutons une validation simple pour vérifier si le champ n'est pas vide.

```js
function validate(input) {
  return input.length > 0;
}
```
Cette fonction validate vérifie simplement si le champ n'est pas vide. Si la longueur du texte est supérieure à 0, la fonction renvoie true (valide).

Ajoutons maintenant un message d'erreur si la validation échoue.

```js
const [errorMessage, setErrorMessage] = useState('');

useEffect(() => {
  if (!validate(value)) {
    setErrorMessage('Le champ ne peut pas être vide.');
  } else {
    setErrorMessage('');
  }
}, [value]);

```
Le hook `useEffect` est utilisé pour exécuter du code à l'initialisation et à la destruction du composant.

Ici, nous voulons vérifier la validité de l'entrée chaque fois que celle-ci change. Le tableau de dépendances `[value]` à la fin du hook dit à React de n'exécuter la fonction à l'intérieur de useEffect **que lorsque inputValue change**.

Modifions notre formulaire pour afficher un message d'erreur.

```js
return (
  <form onSubmit={handleSubmit}>
    <input
      type="text"
      value={inputValue}
      onChange={(e) => setInputValue(e.target.value)}
    />
    {errorMessage && <div>{errorMessage}</div>}
    <button type="submit">Envoyer</button>
  </form>
);
```

Nous utilisons ici l'affichage conditionnel :

```js
errorMessage && <div>{errorMessage}</div>
```

> Si `errorMessage` a une valeur qui n'est pas falsy (pas vide), alors affiche le message dans un <div>. Si errorMessage est vide (pas d'erreur), rien n'est affiché.

### 3 - Le hook 'useForm'

Selon le nombre et la complexité des formulaires dans une application, gérer tous les retours utilisateurs peut vite devenir compliqué.

Il existe de nombreuses librairies pour nous aider à gérer des formulaires dans React : voyons le fonctionnement de ['react-hook-form']('https://react-hook-form.com/get-started').

Il faut d'abord l'installer dans le projet :

```bash
npm install react-hook-form
```

Ensuite, le formulaire avec validation pourra ressembler à celui-ci:

```js
// on importe la librairie
import { useForm } from "react-hook-form"

export default function MyForm() {
  // useForm est un hook de react-hook-form
  const { register, handleSubmit } = useForm();
  // register permet d'enregistrer et gérer des inputs
  // handleSubmit gère la soumission du formulaire
  const onSubmit = (data) => console.log(data);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input {...register("firstName", { required: true, maxLength: 20 })} />
      <input {...register("lastName", { pattern: /^[A-Za-z]+$/i })} />
      <input type="number" {...register("age", { min: 18, max: 99 })} />
      <input type="submit" />
    </form>
  )
}
```

### 4 - À toi de jouer !

Tu dois maintenant créer un formulaire d'inscription **contrôlé, sans react-hook-form** pour recueillir le nom, le prénom, l'âge, l'adresse email et le mot de passe d'un nouvel utilisateur.

Dans la fonction de soumission du formulaire, fais juste un `console.log()` des données recueillies.

Tu devras créer une fonction de contrôle sur l'âge de la personne. L'âge devra être supérieur à 18 ans. Si l'âge est inférieur, un message doit apparaître dans le formulaire.

Tu peux ajouter d'autres contrôles sur le formulaire (email valide, mot de passe) si tu le souhaites.

## Modalités pédagogiques

- Créer un nouveau projet vite / react
- Ajouter un composant 'Form.jsx' dans 'App.jsx'
- Vérifier qu'aucune erreur n'apparaît dans le terminal
- Un dépôt GitLab contient le code du projet

## Modalités d'évaluation

- Les données du formulaire s'affichent dans la console
- Les messages d'erreur s'affichent correctement
- Des commentaires expliquent le code
- Aucune erreur n'apparaît dans le terminal

## Livrables

- Un lien vers un dépôt GitLab

## Critères de performance

- Le code source est documenté
- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions