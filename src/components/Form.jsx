import React, {useState, useEffect} from "react";
import "./Form.css";

export default function Form() {
    // On définit les useState pour chacun de nos input
  const [name, setName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [age, setAge] = useState("");
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");

  // La fonction handleSubmit() sera appelée lors de la validation du formulaire
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(`Le nom saisi est : ${name}`);
    console.log(`Le prénom saisi est : ${firstName}`);
    console.log(`L'âge saisi est : ${age} ans`);
    console.log(`L'adresse email saisie est : ${mail}`);
    console.log(`Le mot de passe saisi est : ${password}`);
  };

  // Pour vérifier que l'utilisateur est majeur
  function validateAge(input) {
    return input >= 18;
  }

  const [ageErrorMessage, setAgeErrorMessage] = useState("");

  useEffect(() => {
    /* Se lance la première fois que le composant est monté et à chaque mise à jour de age. */
    if (age && !validateAge(age)) {
      /* Du coup on vérifie que le champ age n'est pas vide avant de vérifier s'il est supérieur égal à 18 */
      setAgeErrorMessage("Vous devez être majeur !");
    } else {
      setAgeErrorMessage("");
    }
  }, [age]);

  // Le formulaire
  return (
    <form className="my-form" onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="name">Votre nom</label>
        <input
          id="name"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="firstName">Votre prénom</label>
        <input
          id="firstName"
          type="text"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="age">Votre âge</label>
        <input
          id="age"
          type="number"
          value={age}
          onChange={(e) => setAge(e.target.value)}
        />
        {ageErrorMessage && (
          <div className="error-message">{ageErrorMessage}</div>
          /* Attention la première fois que le composant est rendu, la vérification de l'age est effectuée. Donc avant qu'une valeur n'ait pu être entrée, le message "vous devez être majeur" pourrait s'afficher. Il ne disparaitra que quand une valeur entrée supérieure égale à 18 sera entrée. Pour contrer ça, dans useEffect je vérifie également si age est true, donc si une valeur a été entrée */
        )}
      </div>
      <div className="form-group">
        <label htmlFor="mail">Votre email</label>
        <input
          id="mail"
          type="email"
          value={mail}
          onChange={(e) => setMail(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="password">Votre mot de passe</label>
        <input
          id="password"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <button type="submit">Envoyer</button>
    </form>
  );
}
